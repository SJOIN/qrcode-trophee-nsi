function generateQR() {
    const qrSize = document.getElementById("qrSize").value;
    const nom = document.getElementById("Nom").value;
    const IODA = document.getElementById("IODA").value;
    const Safran = document.getElementById("Safran").value;
    const Derive = document.getElementById("Derive").value;

    // Vérifier si toutes les informations requises sont remplies
    if (nom && IODA && Safran && Derive) {
        const background = document.getElementById("background").value;
        const backgroundAlpha = parseFloat(document.getElementById("backgroundAlpha").value);
        const foreground = document.getElementById("foreground").value;
        const foregroundAlpha = parseFloat(document.getElementById("foregroundAlpha").value);

        //Concatène les données remplies dans une variable concatenatedValue
        const concatenatedValue = nom + ", " + IODA + ", " + Safran + ", "  + Derive;

        const qr = new QRious({
            value: concatenatedValue,
            size: qrSize,
            background: background,
            backgroundAlpha: backgroundAlpha,
            foreground: foreground,
            foregroundAlpha: foregroundAlpha
        });

        // Supprimez l'ancien QR Code s'il y en a un
        const qrCodeDiv = document.getElementById("qrCode");
        while (qrCodeDiv.firstChild) {
            qrCodeDiv.removeChild(qrCodeDiv.firstChild);
        }   

        qrCodeDiv.appendChild(qr.image);

        // Créez un lien de téléchargement du QR Code
        const downloadButton = document.getElementById("downloadButton");
        downloadButton.href = qr.toDataURL("image/png");
    } else {
        alert("Veuillez remplir toutes les informations nécessaires avant de générer le QR code.");
    }
}


function resetForm() {
    // Réinitialisez les valeurs des champs de formulaire
    document.getElementById("Nom").value = "";
    document.getElementById("IODA").value = "";
    document.getElementById("Safran").value = "";
    document.getElementById("Derive").value = "";
    document.getElementById("background").value = "#ffffff";
    document.getElementById("backgroundAlpha").value = "0";
    document.getElementById("foreground").value = "#000000";
    document.getElementById("foregroundAlpha").value = "1";

    // Supprimez le QR Code actuel
    const qrCodeDiv = document.getElementById("qrCode");
    while (qrCodeDiv.firstChild) {
        qrCodeDiv.removeChild(qrCodeDiv.firstChild);
    }

    // Réinitialisez le lien de téléchargement
    const downloadButton = document.getElementById("downloadButton");
    downloadButton.removeAttribute("href");

    // Réinitialisez la valeur du range
    const qrSizeInput = document.getElementById("qrSize");
    qrSizeInput.value = "300";
    const qrSizeValue = document.getElementById("qrSizeValue");
    qrSizeValue.innerText = "Taille : 300 pixels";
}


// Vérifiez si la valeur est enregistrée dans le stockage local
if (localStorage.getItem("qrSizeValue")) {
    const savedValue = parseInt(localStorage.getItem("qrSizeValue"));
    qrSizeInput.value = savedValue;
    qrSizeValue.innerText = "Taille : " + savedValue + " pixels";
}

// Ajoutez un gestionnaire d'événements pour mettre à jour la valeur
qrSizeInput.addEventListener("input", function () {
    const value = qrSizeInput.value;
    qrSizeValue.innerText = "Taille : " + value + " pixels";

    // Enregistrez la valeur dans le stockage local
    localStorage.setItem("qrSizeValue", value);
});

function printQR() {
    const qrCodeDiv = document.getElementById("qrCode");
    const printWindow = window.open('', '', 'width=1200,height=1200');

    // Créez une fenêtre pop-up pour l'impression
    printWindow.document.open();
    printWindow.document.write('<html><head><title>QR Code et informations</title>');
    printWindow.document.write('<style>');
    printWindow.document.write('body { font-family: "Calibri", sans-serif; }');
    printWindow.document.write('</style>');
    printWindow.document.write('</head><body>');
    printWindow.document.write('<h1>Informations saisies :</h1>');
    printWindow.document.write('<p><strong>Numéro de voile:</strong> ' + document.getElementById("Nom").value + '</p>');
    printWindow.document.write('<p><strong>Identification IODA:</strong> ' + document.getElementById("IODA").value + '</p>');
    printWindow.document.write('<p><strong>Numéro d’identification du safran:</strong> ' + document.getElementById("Safran").value + '</p>');
    printWindow.document.write('<p><strong>Numéro d’identification de la dérive:</strong> ' + document.getElementById("Derive").value + '</p>');
    printWindow.document.write('<h1>QR Code:</h1>');
    printWindow.document.write(qrCodeDiv.innerHTML);
    printWindow.document.write('</body></html>');
    printWindow.document.close();

    // Attendez que le contenu soit chargé, puis imprimez
    printWindow.onload = function () {
        printWindow.print();
        printWindow.close();
    };
}